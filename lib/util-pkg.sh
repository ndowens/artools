#!/hint/bash

#{{{ pkg conf

load_pkg_config(){

    local conf="$1/artools-pkg.conf"

    [[ -f "$conf" ]] || return 1

    # shellcheck source=/etc/artools/artools-pkg.conf
    [[ -r "$conf" ]] && . "$conf"

    local git_domain="gitea.artixlinux.org"

    GIT_URL=${GIT_URL:-"https://${git_domain}"}

    GIT_SSH=${GIT_SSH:-"gitea@${git_domain}"}

    GIT_TOKEN=${GIT_TOKEN:-''}

    TREE_DIR_ARTIX=${TREE_DIR_ARTIX:-"${WORKSPACE_DIR}/artixlinux"}

    [[ -z ${ARTIX_TREE[*]} ]] && \
    ARTIX_TREE=(main initsys desktop addons lib32)

    TREE_DIR_ARCH=${TREE_DIR_ARCH:-"${WORKSPACE_DIR}/archlinux"}

    [[ -z ${ARCH_TREE[*]} ]] && \
    ARCH_TREE=(svntogit-{packages,community})

    REPOS_ROOT=${REPOS_ROOT:-"${WORKSPACE_DIR}/repos"}

    REPOS_MIRROR=${REPOS_MIRROR:-'http://mirror1.artixlinux.org/repos'}

    HOST_TREE_ARCH=${HOST_TREE_ARCH:-'https://github.com/archlinux'}

    DBEXT=${DBEXT:-'gz'}

    return 0
}

#}}}

load_pkg_config "${USER_CONF_DIR}" || load_pkg_config "${SYSCONFDIR}"

prepare_dir "${REPOS_ROOT}"
prepare_dir "${TREE_DIR_ARTIX}"
prepare_dir "${TREE_DIR_ARCH}"
