#!/hint/bash

#{{{ jenkins

write_jenkinsfile(){
    local pkg="$1"
    local jenkins=$pkg/Jenkinsfile

    printf "@Library('artix-ci') import org.artixlinux.RepoPackage\n" > "$jenkins"
    {
    printf '\n'
    printf 'PackagePipeline(new RepoPackage(this))\n'
    printf '\n'
    } >> "$jenkins"

    git add "$jenkins"
}

write_agentyaml(){
    local pkg="$1"
    local agent="$pkg"/.artixlinux/agent.yaml label='master'
    [[ -d $pkg/.artixlinux ]] || mkdir "$pkg"/.artixlinux

    printf '%s\n' '---' > "$agent"
    {
    printf '\n'
    printf "label: %s\n" "$label"
    printf '\n'
    } >> "$agent"

    git add "$agent"
}

commit_ci(){
    local pkg="$1"

    write_jenkinsfile "$pkg"
    write_agentyaml "$pkg"

    git commit -m "initial ci commit"
}

#}}}
