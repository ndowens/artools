#!/hint/bash

##{{{ repo

shopt -s extglob

load_valid_names(){
    local conf
    conf="${DATADIR}"/valid-names.conf
    [[ -f "$conf" ]] || return 1
    # shellcheck source=/usr/share/artools/valid-names.conf
    [[ -r "$conf" ]] && . "$conf"
    return 0
}

set_arch_repos(){
    local _testing="$1" _staging="$2" _unstable="$3"
    [[ -z ${valid_names[*]} ]] && load_valid_names
    ARCH_REPOS=("${stable[@]}")
    $_testing && ARCH_REPOS+=("${gremlins[@]}")
    $_staging && ARCH_REPOS+=("${goblins[@]}")
    $_unstable && ARCH_REPOS+=("${wobble[@]}")
}

find_repo(){
    local pkg="$1" pkgarch="${2:-${CARCH}}" repo
    for r in "${ARCH_REPOS[@]}"; do
        [[ -f $pkg/repos/$r-$pkgarch/PKGBUILD ]] && repo="$r-$pkgarch"
        [[ -f $pkg/repos/$r-any/PKGBUILD ]] && repo="$r"-any
        [[ -f $pkg/$pkgarch/$r/PKGBUILD ]] && repo="$r"
    done
    printf "%s\n" "$repo"
}

find_pkg(){
    local searchdir="$1" pkg="$2" result
    result=$(find "$searchdir" -mindepth 2 -maxdepth 2 -type d -name "$pkg")
    printf "%s\n" "$result"
}

tree_loop(){
    local func="$1" pkgs
    for tree in "${ARTIX_TREE[@]}"; do
        mapfile -t pkgs < <(find "${TREE_DIR_ARTIX}/$tree" -name "$CARCH" | sort)
        for p in "${pkgs[@]}"; do
            "$func" "$p"
        done
    done
}

#}}}
