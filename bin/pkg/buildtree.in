#!/bin/bash
#
# Copyright (C) 2018-19 artoo@artixlinux.org
# Copyright (C) 2018 Artix Linux Developers
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

m4_include(lib/util-base.sh)
m4_include(lib/util-pkg.sh)
m4_include(lib/base/message.sh)
m4_include(lib/pkg/common.sh)
m4_include(lib/pkg/git.sh)
m4_include(lib/pkg/gitea.sh)
m4_include(lib/pkg/repo.sh)
m4_include(lib/pkg/jenkins.sh)

#{{{ new git repo

pkgrepo_new(){
    cd "${TREE_DIR_ARTIX}/${group}" || return

    local org gitname
    org=$(get_pkg_org "${package}")
    gitname=$(get_compliant_name "${package}")

    if [[ ! -d "${package}" ]]; then
        gitearepo -cr "$gitname"

        add_team_to_repo "$gitname" "$org" "${team}"

        msg2 "Adding [%s] from (%s)" "${package}" "$org/$gitname"
        braid add "${GIT_SSH}":"$org/$gitname".git "${package}"

        prepare_dir "${TREE_DIR_ARTIX}/${group}/${package}/trunk"

        commit_ci "${package}"

#         if braid push "${package}"; then
#             braid update "${package}"
#         fi
    else
        die "Package %s already exists!" "${package}"
    fi
}

#}}}

#{{{ pull

config_tree() {
    local dist="$1" vars="${2:-false}"
    case "$dist" in
        Artix)
            if "$vars"; then
                dist_tree=("${ARTIX_TREE[@]}")
                dist_url="${GIT_SSH}:pkgbuild"
                dist_dir=${TREE_DIR_ARTIX}
            else
                git config --bool pull.rebase true
                git config commit.gpgsign true
                if [[ -n "${GPGKEY}" ]];then
                    git config user.signingkey "${GPGKEY}"
                else
                    warning "No GPGKEY configured in makepkg.conf!"
                fi
            fi
        ;;
        Arch)
            if "$vars"; then
                dist_tree=("${ARCH_TREE[@]}")
                dist_url="${HOST_TREE_ARCH}"
                dist_dir=${TREE_DIR_ARCH}
            else
                git config --bool pull.rebase false
                git config branch.master.mergeoptions --no-edit
            fi
        ;;
    esac
}

update_tree() {
    local dist="${1:-Artix}"
    config_tree "$dist" true
    cd "$dist_dir" || return
    for tree in "${dist_tree[@]}"; do
        if [[ -d "$tree" ]]; then
            ( cd "$tree" || return
                config_tree "$dist"
                msg "Checking (%s) (%s)" "$tree" "$dist"
                pull_tree "$tree" "$(get_local_head)"
            )
        else
            msg "Cloning (%s) (%s) ..." "$tree" "$dist"
            git clone "$dist_url/$tree".git
            ( cd "$tree" || return
                config_tree "$dist"
            )
        fi
    done
}

#}}}

#{{{ patch

set_maintainer() {
    local name email path="$1"
    name=$(git -C $path config --get user.name)
    email=$(git -C $path config --get user.email)
    sed -e "1s|Maintainer:.*|Maintainer: $name <$email>|" \
        -i "$path"/PKGBUILD
}

patch_pkg(){
    local pkgpath="$1"
    local pkg=${pkgpath%/*}
    pkg=${pkg##*/}
    sed -e 's|arch-meson|artix-meson|' -i "$pkgpath"/PKGBUILD
    set_maintainer "$pkgpath"
    case $pkg in
        glibc)
            msg2 "Patching %s" "$pkg"
            sed -e 's|{locale,systemd/system,tmpfiles.d}|{locale,tmpfiles.d}|' \
                -e '/nscd.service/d' \
                -i "$pkgpath"/PKGBUILD
        ;;
        linux|linux-lts)
            msg2 "Patching %s" "$pkg"
            sed -e 's|KBUILD_BUILD_HOST=.*|KBUILD_BUILD_HOST=artixlinux|' -i "$pkgpath"/PKGBUILD
            sed -e 's|CONFIG_DEFAULT_HOSTNAME=.*|CONFIG_DEFAULT_HOSTNAME="artixlinux"|' \
                -i "$pkgpath"/config
        ;;
        bash)
            msg2 "Patching %s" "$pkg"
            # shellcheck disable=2016
            sed -e 's|system.bash_logout)|system.bash_logout artix.bashrc)|' \
            -e "s|etc/bash.|etc/bash/|g" \
            -e 's|"$pkgdir/etc/skel/.bash_logout"|"$pkgdir/etc/skel/.bash_logout"\n  install -Dm644 artix.bashrc $pkgdir/etc/bash/bashrc.d/artix.bashrc|' \
            -i "$pkgpath"/PKGBUILD
            ( cd "$pkgpath" || return
                patch -Np 1 -i "${DATADIR}"/patches/artix-bash.patch
                updpkgsums
            )
        ;;
        gstreamer|gst-plugins-*|licenses)
            msg2 "Patching %s" "$pkg"
            sed -e 's|https://www.archlinux.org/|https://www.artixlinux.org/|' \
                -e 's|(Arch Linux)|(Artix Linux)|' \
                -i "$pkgpath"/PKGBUILD
        ;;
    esac
}

#}}}

#{{{ pkgbuild import

sync_pkg(){
    local rsync_args=(-aWxvci --progress --delete-before --no-R --no-implied-dirs)
    local src="$1" dest="$2"
    msg "Sync from Arch [%s] to Artix [%s]" "${src##*archlinux/}" "${dest##*artixlinux/}"
    rsync "${rsync_args[@]}" "$src"/ "$dest"/
}

show_deps(){
    local src="$1" archver

    # shellcheck disable=1090
    . "$src"/PKGBUILD 2>/dev/null

    archver=$(get_full_version)

    # shellcheck disable=2154
    [[ -n ${pkgbase} ]] && msg2 "pkgbase: %s" "${pkgbase}"
    # shellcheck disable=2154
    msg2 "pkgname: %s" "${pkgname[*]}"
    # shellcheck disable=2154
    [[ -n "${pkgdesc}" ]] && msg2 "pkgdesc: %s" "${pkgdesc}"
    # shellcheck disable=2154
    msg2 "Arch Version: %s" "$archver"
    # shellcheck disable=2154
    msg2 "arch: %s" "$arch"
    # shellcheck disable=2154
    [[ -n ${makedepends[*]} ]] && msg2 "makedepends: %s" "${makedepends[*]}"
    # shellcheck disable=2154
    [[ -n ${checkdepends[*]} ]] && msg2 "checkdepends: %s" "${checkdepends[*]}"
    # shellcheck disable=2154
    [[ -n ${depends[*]} ]] && msg2 "depends: %s" "${depends[*]}"
    # shellcheck disable=2154
    [[ -n ${optdepends[*]} ]] && msg2 "optdepends: %s" "${optdepends[@]}"
}

from_arch(){
    cd "${TREE_DIR_ARTIX}" || return

    local srcpath repo archpath artixpath
    srcpath=$(find_pkg "${TREE_DIR_ARCH}" "${package}")
    [[ -z $srcpath ]] && die "Package '%s' does not exist!" "${package}"

    repo=$(find_repo "$srcpath")
    msg "repo: %s" "$repo"

    archpath=$srcpath/repos/$repo
    artixpath=$(find_pkg "${TREE_DIR_ARTIX}" "${package}")

    show_deps "$archpath"

    if [[ -f "$artixpath"/Jenkinsfile ]]; then
        artixpath="$artixpath"/trunk

        if [[ -d "$artixpath" ]];then
            sync_pkg "$archpath" "$artixpath"
            patch_pkg "$artixpath"
        fi
    else
        die "Package '%s' does not exist!" "${package}"
    fi
}

#}}}

view_build(){
    local archpath repo
    archpath=$(find_pkg "${TREE_DIR_ARCH}" "${package}")
    [[ -z $archpath ]] && die "Package '%s' does not exist!" "${package}"
    repo=$(find_repo "${archpath}")
    archpath=$archpath/repos/$repo
    msg "repo: %s" "${repo#*/}"
    show_deps "$archpath"
}

check_tree(){
    local archpath artixpath group
    archpath=$(find_pkg "${TREE_DIR_ARCH}" "${package}")
    [[ -z $archpath ]] && die "Package '%s' does not exist!" "${package}"
    artixpath=$(find_pkg "${TREE_DIR_ARTIX}" "${package}")
    group=${artixpath%/*}
    msg "group: %s" "${group##*/}"

    [[ -z $artixpath ]] && msg2 "exists: %s" "no"
    [[ -n $artixpath ]] && msg2 "exists: %s" "yes"
}

sync_repos(){
    ${sync_arch} && update_tree "Arch"
    ${sync_artix} && update_tree
}

load_makepkg_config

load_valid_names

testing=true
staging=true
unstable=false

sync=false
sync_arch=true
sync_artix=true
import=false
view=false
createnew=false
check=false

package=''

team='community'
group="${ARTIX_TREE[0]}"

usage() {
    printf "Usage: %s [options]\n" "${0##*/}"
    printf '    -p <pkg>      Package name\n'
    printf '    -t <team>     Team name (only with -n)\n'
    printf '                  Possible values: core,extra,community,multilib\n'
    printf "                  [default: %s]\n" "${team}"
    printf '    -g <group>    Group name, the superrepo (only with -n)\n'
    printf "                  [default: %s]\n" "${group}"
    printf "    -s            Clone or pull repos\n"
    printf "    -a            Don't clone or pull arch repos\n"
    printf "    -b            Don't clone or pull artix repos\n"
    printf '    -i            Import a package from arch repos\n'
    printf '    -n            New remote pkgrepo and add it\n'
    printf '    -v            Check if a package is in the artix tree(s)\n'
    printf '    -c            View package depends\n'
    printf '    -x            Exclude testing (only with -i)\n'
    printf '    -y            Exclude staging (only with -i)\n'
    printf '    -z            Include kde & gnome unstable (only with -i)\n'
    printf '    -h            This help\n'
    printf '\n'
    printf '\n'
    exit "$1"
}

opts='p:t:g:sinabcvxyzh'

while getopts "${opts}" arg; do
    case "${arg}" in
        p) package="$OPTARG" ;;
        t) team="$OPTARG" ;;
        g) group="$OPTARG" ;;
        s) sync=true ;;
        a) sync_arch=false ;;
        b) sync_artix=false ;;
        i) import=true ;;
        n) createnew=true ;;
        c) check=true ;;
        v) view=true ;;
        x) testing=false ;;
        y) staging=false ;;
        z) unstable=true ;;
        h|?) usage 0 ;;
    esac
done

shift $(( OPTIND - 1 ))

set_arch_repos "$testing" "$staging" "$unstable"

${sync} && sync_repos

${view} && view_build

${check} && check_tree

${import} && from_arch

${createnew} && pkgrepo_new
